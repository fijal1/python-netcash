"""
Warning, this runs as an integration test
"""
import unittest
from netcash.client import Netcash
from datetime import date
import os

NETCASH_MERCHANT_ACCOUNT = os.environ.get("NETCASH_MERCHANT_ACCOUNT")
NETCASH_SOFTWARE_VENDOR_KEY = os.environ.get("NETCASH_SOFTWARE_VENDOR_KEY")

NETCASH_SERVICE_ID = os.environ.get("NETCASH_SERVICE_ID")
NETCASH_SERVICE_KEY = os.environ.get("NETCASH_SERVICE_KEY")


class TestIntegration(unittest.TestCase):
    def setUp(self):

        self.netcash = Netcash(NETCASH_MERCHANT_ACCOUNT, NETCASH_SOFTWARE_VENDOR_KEY,)

    def test_config(self):
        assert NETCASH_MERCHANT_ACCOUNT is not None
        assert NETCASH_SERVICE_ID is not None
        assert NETCASH_SERVICE_KEY is not None
        assert NETCASH_SOFTWARE_VENDOR_KEY is not None

    def test_validate_service_key(self):
        res = self.netcash.validate_service_key(
            service_id=NETCASH_SERVICE_ID, service_key=NETCASH_SERVICE_KEY
        )
        assert res is True, "Expected to return true for a valid service_key"

    def test_request_statement(self):
        dt = date.today()
        polling_id = self.netcash.request_merchant_statement(
            from_date=dt, service_key=NETCASH_SERVICE_KEY
        )
        assert polling_id is not None

        res = self.netcash.retrieve_merchant_statement(
            polling_id, service_key=NETCASH_SERVICE_KEY, as_csv_reader=True
        )
        for row in res:
            print(row)
        # somehow assert hsi is a csvreader?


if __name__ == "__main__":
    unittest.main()
