from .payloads import (
    ValidateServiceKey,
    RequestMerchantStatement,
    RetrieveMerchantStatement,
    GetBankListWithDefaultBranchCode,
    ValidateBankAccount
)
from benedict import benedict as bdict
from twisted.internet.defer import inlineCallbacks, returnValue
from io import StringIO
from datetime import date
import treq
import csv


class NetcashError(Exception):
    pass


class Netcash:

    NIWS_INBOUND = "https://ws.netcash.co.za/NIWS/niws_nif.svc"
    NIWS_PARTNER = "https://ws.netcash.co.za/NIWS/NIWS_Partner.svc"
    VALIDATION = "https://ws.netcash.co.za/NIWS/niws_validation.svc"
    STATEMENT_SERVICE = "https://ws.netcash.co.za/NIWS/NIWS_NIF.svc"

    def __init__(self, merchant_account, software_vendor_id):
        self.merchant_account = merchant_account
        self.software_vendor_id = software_vendor_id

    def _get_soap_result(self, raw_result, soap_action):
        print(raw_result)
        data = bdict.from_xml(raw_result.decode("utf-8"))
        return (
            data.get("s:Envelope")
            .get("s:Body")
            .get("{}Response".format(soap_action))
            .get("{}Result".format(soap_action))
        )

    def validate_service_key(self, service_id, service_key):
        """
        Returns true or false
        """
        url = self.NIWS_PARTNER
        body = ValidateServiceKey.format(
            **{
                "software_vendor_id": self.software_vendor_id,
                "merchant_account": self.merchant_account,
                "service_id": service_id,
                "service_key": service_key,
            }
        )
        headers = {
            "Content-Type": 'application/soap+xml;charset=UTF-8;action="http://tempuri.org/INIWS_Partner/ValidateServiceKey"',
        }

        result = requests.post(url, body, headers=headers)
        return "<b:ServiceStatus>001</b:ServiceStatus>" in result.content.decode(
            "utf-8"
        )

    def request_merchant_statement(self, from_date, service_key):
        url = self.STATEMENT_SERVICE
        date_formatted = from_date.strftime("%Y%m%d")
        options = {"service_key": service_key, "from_date": date_formatted}
        body = RequestMerchantStatement.format(**options)
        print(body)
        headers = {
            "Content-Type": "text/xml;charset=UTF-8",
            "SOAPAction": "http://tempuri.org/INIWS_NIF/RequestMerchantStatement",
        }

        res = requests.post(url, body, headers=headers)
        if res.ok:
            return self.__get_soap_result(res.content, "RequestMerchantStatement")

    def get_bank_list(self, service_key):
        url = self.VALIDATION
        body = GetBankListWithDefaultBranchCode.format(**{"service_key": service_key})
        headers = {
            "Content-Type": "text/xml;charset=UTF-8",
            "SOAPAction": "http://tempuri.org/INIWS_Validation/GetBankListWithDefaultBranchCode"
        }

        res = requests.post(url, body, headers=headers)
        if not res.ok:
            raise NetcashError("Error getting bank list: %s" % res.text)
        return self._get_soap_result(res.content, "GetBankListWithDefaultBranchCode")

    @inlineCallbacks
    def validate_bank_account(self, service_key, account_number, branch_code):
        url = self.VALIDATION
        account_type = "1"
        if branch_code == 198765 or branch_code == 720026:
            if account_number[0] == "1":
                branch_code = 198765
            elif account_number[0] == "2":
                branch_code = 198765
                account_type = "2"
            elif account_number[0] == "9":
                branch_code = 720026 
                account_type = "2"
        elif branch_code == 460005:
            account_type = "2"
        body = ValidateBankAccount.format(**{"service_key": service_key,
            "account_type": account_type, "branch_code": str(branch_code),
            "account_number": account_number})
        headers = {
            "Content-Type": "text/xml;charset=UTF-8",
            "SOAPAction": "http://tempuri.org/INIWS_Validation/ValidateBankAccount"
        }

        res = yield treq.post(url, body, headers=headers)
        if res.code != 200:
            raise NetcashError("Error verifying bank account: %s" % res.text)
        content = yield res.content()
        r = self._get_soap_result(content, "ValidateBankAccount")
        if r == '0':
            returnValue(False)
        else:
            returnValue(True)

    def retrieve_merchant_statement(self, polling_id, service_key, as_csv_reader = True):
        url = self.STATEMENT_SERVICE
        body = RetrieveMerchantStatement.format(
            **{"service_key": service_key, "polling_id": polling_id}
        )
        print(body)
        headers = {
            "Content-Type": "text/xml;charset=UTF-8",
            "SOAPAction": "http://tempuri.org/INIWS_NIF/RetrieveMerchantStatement",
        }

        res = requests.post(url, body, headers=headers)
        if res.ok:
            csvdata = self.__get_soap_result(res.content, "RetrieveMerchantStatement")
            if as_csv_reader:
                f = StringIO(csvdata)
                return csv.reader(f, delimiter="\t")
            return csvdata
